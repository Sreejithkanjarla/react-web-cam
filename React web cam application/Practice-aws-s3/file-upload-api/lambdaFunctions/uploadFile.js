

const AWS = require('aws-sdk');
const accessKeyId =  "AKIA56D5TOEBUEFIJA4Y" ;
const secretAccessKey = "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";

AWS.config.update({
  accessKeyId: accessKeyId,
  secretAccessKey: secretAccessKey
});

var s3 = new AWS.S3();

const uploadFiles =  async (req, res) => {


    
    console.log("reqBody", req.body);
    console.log("reqfile", req.file);
    console.log("req", req.apiGateway.event.body);

    if (req.file) {

        const {originalname, buffer, mimetype} = req.file;

        let params = { 
            Bucket: "file-upload-api-dev-serverlessdeploymentbucket-1pt7e4n5b748",
            Key: originalname,
            Body: buffer,
        };

        s3.upload(params, (err, result) => {
            if(err) {
                res.status(500).json({
                    message: "Failed to upload",
                    error: err.message
                });
            }
    
            res.status(201).json({
                message: "File uploaded",
                result
            });
    
        });

    } else {


        const buffer = new Buffer.from(req.apiGateway.event.body.split(/^data:image\/\w+;base64,/, ""), 'base64');

        const type = req.apiGateway.event.body.split(';')[0].split('/')[1];

        let params = { 
        Bucket: "file-upload-api-dev-serverlessdeploymentbucket-1pt7e4n5b748",
        Key: `${req.apiGateway.context.awsRequestId}.${type}`,
        Body: buffer,
        ContentEncoding: 'base64',
        ContentType: 'image/jpeg'
        };

        s3.upload(params, (err, result) => {
            if(err) {
                res.status(500).json({
                    message: "Failed to upload",
                    error: err.message
                });
            }
    
            res.status(201).json({
                message: "File uploaded",
                result
            });
    
        });
    }

}

module.exports = {
    uploadFiles,
};