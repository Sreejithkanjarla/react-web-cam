const serverless = require("serverless-http");
const express = require("express");
const cors = require("cors");
const bodyparser = require("body-parser");
const multer = require("multer");

const uploadfunction = require("./lambdaFunctions/uploadFile");
const uploadImageFunction = require("./lambdaFunctions/uploadImage");
const app = express();
const multerUpload = multer();

const AWS = require('aws-sdk');
const accessKeyId =  "AKIA56D5TOEBUEFIJA4Y" ;
const secretAccessKey = "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";

AWS.config.update({
  accessKeyId: accessKeyId,
  secretAccessKey: secretAccessKey
});

app.use(cors());
app.use(bodyparser.json());

app.get("/", (req, res, next) => {
  return res.status(200).json({
    message: "Hello from root!",
  });
});

app.get("/hello", (req, res, next) => {
  return res.status(200).json({
    message: "Hello from path!",
  });
});

app.post("/upload", multerUpload.single("file") , uploadfunction.uploadFiles);
app.post("/uploadImage", multerUpload.single("file"), uploadImageFunction.uploadImages);

app.use((req, res, next) => {
  return res.status(404).json({
    error: "Not Found",
  });
});

module.exports.handler = serverless(app);
